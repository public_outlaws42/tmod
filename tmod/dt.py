#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Functions to work with date and time.
"""

# Standard Library Imports
from datetime import datetime, date, time

# Imports from tmods

from messages_warn import import_mod_message_from


# Imports installed through pip
try:
    # pip install pytz if needed
    from pytz import timezone
except ModuleNotFoundError:
    message = import_mod_message_from(
        imported="timezone",
        ifrom="pytz",
        methods=["from_str_time_meridiem()", "from_str_time", "from_str_date"],
    )
    print(message)


__author__ = "Troy Franks"
__version__ = "2023-04-02"


def day_diff(
    month: int,
    day: int,
    year: int,
):
    """
    Takes a date month, day, year. Outputs the
    difference between that date and todays date
    Requires: from datetime import date
    """
    current = date.today()
    day_target = date(year, month, day)
    till_day = day_target - current
    return till_day.days


def year_current():
    """
    Returns current year
    """
    current = date.today()
    current_year = current.year
    return current_year


def date_current():
    """
    Returns current date
    """
    current = date.today()
    current_date = current
    return current_date


def time_now() -> str:
    """
    Returns current string time
    """
    current = datetime.now().strftime("%H:%M:%S")
    return current


def str_date_from_datetime(dt: datetime):
    """
    Datetime return string date
    Requires from datetime import datetime
    """
    str_date = dt.strftime("%Y-%m-%d")
    return str_date


def from_str_time_meridiem(
    str_time: str,
    timestamp: bool = False,
    utc: bool = False,
    tzone: str = "US/Eastern",
):
    """
    Takes a string time with AM or PM,
    timestamp = True or False,
    utc = True or False, timezone = 'US/Eastern'
    timestamp = False returns datetime object today at that time,
    timestamp = True returns timestamp today at that time
    utc = True  sets timezone to UTC, False sets timezone to local timezone
    --- Requires ---
    from datetime import datetime, date
    from pytz import timezone
    """

    if utc:
        tz = timezone("UTC")
    else:
        tz = timezone(tzone)
    dt_time = datetime.strptime(str_time, "%I:%M %p").time()
    dt = datetime.combine(date.today(), dt_time)
    dttz = tz.localize(dt)
    if timestamp:
        ts = int(dttz.timestamp())
        return ts
    else:
        return dttz


def from_str_time(
    str_time: str,
    timestamp: bool = False,
    utc: bool = False,
    tzone: str = "US/Eastern",
):
    """
    Pass string time HH:MM,
    timestamp = True or False,
    utc = True or False,
    timezone = 'US/Eastern'
    timestamp = False returns datetime object today at that time,
    timestamp = True returns timestamp today at that time
    utc = True  sets timezone to UTC, False sets timezone to local timezone
    -- Requires --
    from datetime import datetime, date, time
    from pytz import timezone
    """
    if utc:
        tz = timezone("UTC")
    else:
        tz = timezone(tzone)

    hour, minute = str_time.split(":")
    dt = datetime.combine(date.today(), time(int(hour), int(minute)))
    dttz = tz.localize(dt)
    print(dttz)
    if timestamp:
        ts = int(dttz.timestamp())
        return ts
    else:
        return dttz


def from_str_date(
    str_date: str,
    timestamp: bool = False,
    utc: bool = False,
    tzone: str = "US/Eastern",
):
    """Pass string date YYYY-MM-DD,
    timestamp = True or False,
    utc = True or False,
    timezone = 'US/Eastern'
    timestamp = False returns datetime object at the date at current time,
    timestamp = True returns timestamp at that date at the current time
    utc = True  sets timezone to UTC, False sets timezone to local timezone
    -- Requires --
    from datetime import datetime, date, time
    from pytz import timezone
    """
    if utc:
        tz = timezone("UTC")
    else:
        tz = timezone(tzone)
    date_request = str_date
    year, month, day = date_request.split("-")
    dt = datetime.combine(date(int(year), int(month), int(day)), time())
    dttz = tz.localize(dt)
    if timestamp:
        ts = int(dttz.timestamp())
        return ts
    else:
        return dttz


# diff = date_current()

diff = from_str_time_meridiem(
    str_time="10:25pm",
    timestamp=False,
    utc=False,
)
print(diff)
