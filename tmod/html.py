#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions dealing with html or web  stuff
"""

from smtplib import SMTP
from bs4 import BeautifulSoup
import requests


__author__ = "Troy Franks"
__version__ = "2023-03-23"


def html_info(tag, url):
    try:
        response = requests.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        find_status = soup.find(tag)
        final_status = find_status.text.strip()
    except requests.exceptions.RequestException as e:
        print(e)
        final_status = "Can't connect"
        print(final_status)
    return final_status


def mail(
    body: str,
    subject: str,
    send_to: list,
    login: list,
):
    """
    body = Body of the message,
    subject = The subject,
    send_to = Who you want to send the message to,
    login = The login information for email.
    Requires from smtplib import SMTP
    """
    us, psw = login
    message = f"Subject: {subject}\n\n{body}"
    print(message)
    try:
        mail = SMTP("smtp.gmail.com", 587)
        mail.ehlo()
        mail.starttls()
        mail.ehlo()
        mail.login(us, psw)
        mail.sendmail(us, send_to, message)
        mail.close()
        print("Successfully sent email")
    except Exception as e:
        print("Could not send email because")
        print(e)
