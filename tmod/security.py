#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Functions to work with security
"""

# Standard Library Imports

# Imports from tmods
from file_io import (
    open_file,
    save_file,
    home_dir,
    open_yaml,
)

from messages_warn import import_mod_message_from


# Imports installed through pip
try:
    # pip install cryptography if needed
    from cryptography.fernet import Fernet
except ModuleNotFoundError:
    message = import_mod_message_from(
        imported="Fernet",
        ifrom="cryptography.fernet",
        methods=["gen_key", "decrypt", "encrypt"],
    )
    print(message)


__author__ = "Troy Franks"
__version__ = "2023-04-02"


def gen_key(
    fname: str,
):
    home = home_dir()
    key = Fernet.generate_key()
    with open(f"{home}/{fname}", "wb") as fkey:
        fkey.write(key)


def decrypt(
    key: str,
    fname: str,
    e_fname: str,
    fdest: str = "relative",
):
    """
    key = key file used to encrypt file,
    fname = file to encrypt,
    e_fname = name of the encrypted file,
    fdest = file destination relative too,
    Takes a encrypted input file and dencrypts output file
    requires the tmod open_file and save_file functions
    requires from cryptography.fernet import Fernet
    """
    keyf = Fernet(key)
    encrypted = open_file(
        fname=e_fname,
        fdest=fdest,
        mode="rb",
    )
    decrypt_file = keyf.decrypt(encrypted)
    save_file(
        fname=fname,
        content=decrypt_file,
        fdest=fdest,
        mode="wb",
    )


def encrypt(
    key: str,
    fname: str,
    e_fname: str,
    fdest="relative",
):
    """
    key = key file used to encrypt file,
    fname = file to encrypt,
    e_fname = name of the encrypted file,
    fdest = file destination relative too,
    Takes a input file and encrypts output file
    requires tmod open_file and save_file functions
    requires from cryptography.fernet import Fernet
    """
    keyf = Fernet(key)
    e_file = open_file(
        fname=fname,
        fdest=fdest,
        mode="rb",
    )
    encrypted_file = keyf.encrypt(e_file)
    save_file(
        fname=e_fname,
        content=encrypted_file,
        fdest=fdest,
        mode="wb",
    )


def decrypt_login(
    key: str,
    e_fname: str,
    fdest: str = "relative",
):
    keyf = Fernet(key)
    encrypted = open_file(
        fname=e_fname,
        fdest=fdest,
        mode="rb",
    )
    decrypt_file = keyf.decrypt(encrypted)
    usr = decrypt_file.decode().split(":")
    return [usr[0], usr[1]]


def login_info(
    fname: str,
    fdest: str = "home",
):
    """
    fname = filename of the file that has the login,
    fdest = The destination of the file
    Get login info from a yaml file
    """
    ps = open_yaml(fname, fdest)
    for key, value in ps.items():
        us = key
        psw = value
    return [us, psw]
