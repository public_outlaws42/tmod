#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Functions to work with doing a wizard to setup configuration files.
"""

# Standard Library Imports
from re import search
from datetime import datetime


# Imports from tmods

from file_io import (
    home_dir,
    check_file_dir,
    make_dir,
    save_yaml,
    remove_file,
    open_file,
)

from security import gen_key, encrypt
from colors import (
    colors,
    cyan_bold,
    green_bold,
    yellow_bold,
    purple_bold,
    red_bold,
    cyan,
)


__author__ = "Troy Franks"
__version__ = "2023-03-24"


# Global Variables


# Input Functions


def input_list(
    subject: str,
    description: str,
    in_type: str = "email",
    outword: str = "next",
):
    """
    subject = The subject of the input item,
    description = the description of the input item,
    in_type = the type of input field. Choices are
    email, file, int, time, password
    outward = This is the word used to present to the user
    to stop adding more items.
    This would be used for a input item that you would
    want to add to a list.
    Requires: doesn't require any special imports
    """
    in_type_error = red_bold(f"This is not a valid {in_type}")
    print(
        (
            f"\n{purple_bold(subject.capitalize())}"
            f"You can add as many items as you like.\n"
            f"But you must add at least 1 item.\n"
            f"When your done adding, Type "
            f"{red_bold(outword)}\n"
        )
    )
    item_list = []
    while True:
        item: str = input(f"Enter the {subject} {description}: ")
        while validate_input(item, in_type) is False:
            if item.upper() == outword.upper() and len(item_list) != 0:
                break
            if item.upper() == outword.upper():
                print(f"{red_bold(f'You need to enter at least 1 {in_type}')}")
            else:
                print(in_type_error)
            item: str = input(f"Enter the {subject} {description}: ")
        if item.upper() == outword.upper() and len(item_list) != 0:
            length = len(item_list)
            print(
                (
                    f"\nYou have added {length} item(s), "
                    f"Because you typed {red_bold(item)}\n"
                    f"That will complete your selection for"
                    f'"{subject.capitalize()}".'
                )
            )
            break
        else:
            print(f"You added {cyan(item)}")
            item_list.append(item)
        print(f"{cyan(item_list)}")
    return item_list


def input_single(
    in_message: str,
    in_type: str = "email",
    fdest: str = "home",
    max_number: int = 200,
):
    """
    in_message = the message you want in your input string,
    in_type = the type of input field. Choices are
    email, file, int, time, password
    This is for a single item input. This uses "validate_input"
    to verify that items entered meet requirements for that type of input
    """
    c = colors()
    if in_type == "int" or in_type == "float":
        item = input(f"{in_message}(Max {max_number}): ")
    else:
        item = input(f"{in_message}: ")
    while (
        validate_input(
            item=item,
            in_type=in_type,
            fdest=fdest,
            max_number=max_number,
        )
        is False
    ):
        print(f"{red_bold(f'This is not a valid {in_type}')}")
        if in_type == "int" or in_type == "float":
            item = input(f"{in_message}(max {max_number}): ")
        else:
            item = input(f"{in_message}: ")
    if in_type == "password":
        print(f'{cyan("******")}')
    else:
        print(f'You entered {c["CYAN"]}{item}{c["END"]}')
    if in_type == "int":
        return int(item)
    elif in_type == "float":
        return float(item)
    else:
        return item


def validate_input(
    item: str,
    in_type: str,
    fdest: str = "home",
    max_number: int = 200,
):
    """
    item = The data entered in the input field,
    email, file, password, int, float, time
    in_type = The type of data it is suposed to be,
    max_number = the max number that can be ablied this if for int only.
    Takes the input and checks to see if it is
    valid for its data type.
    """

    if in_type == "email":
        print(item)
        regex = "^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"
        if search(regex, item):
            return True
        else:
            return False
    elif in_type == "file":
        if not item:
            return False
        else:
            return check_file_dir(item, fdest)
    elif in_type == "password":
        if not item:
            return False
    elif in_type == "time":
        try:
            datetime.strptime(item, "%H:%M").time()
            return True
        except ValueError:
            return False
    elif in_type == "int":
        try:
            number = int(item)
            if (number <= 0) or (number > max_number):
                return False
            return True
        except Exception:
            return False
    elif in_type == "float":
        try:
            number = float(item)
            if (number <= 0) or (number > max_number):
                return False
            return True
        except Exception:
            return False
    else:
        return False


# Wizard setup


def config_setup(
    conf_dir: str,
    conf_file_name: str = "emailog_set",
):
    """
    conf_dir = Configuration dir. This would
    normally be in the .config/progName,
    This commandline wizard creates the
    program config dir and populates the
    setup configuration file. Sets up username
    and password for email notifications
    """
    home = home_dir()
    make_dir(conf_dir)

    no_config = "We could not find any configuration folder\n"
    intro_desc_1 = (
        "This wizard will ask some questions to "
        "setup the\nconfiguration needed for "
        "the script to function\n"
    )
    intro_desc_2 = "This configuration wizard will only run once.\n"
    intro_desc_3 = (
        "The first 2 questions are going to be about your email\n"
        "and password you are using to send.\n"
        "This login information will be stored on your local computer\n"
        "encrypted seperate from the rest of the configuration.\n"
    )

    intro_desc_4 = "This is not viewable by browsing the filesystem\n"
    statement = "\nThis completes the wizard"
    message_1 = (
        f"\nThe configuration file has been written to disk"
        f"\nIf you want to change the settings you can edit "
    )
    conf_dir_str = f"{home}/{conf_dir}/{conf_file_name}.yaml"
    message_2 = (
        f"\nThis wizard "
        f"won't run any more, So the script can "
        f"now be run automatically\n"
    )
    message_3 = f"You can stop the script by typing Ctrl + C\n"

    print(
        (
            f"{yellow_bold(no_config)}\n"
            f"{green_bold(intro_desc_2)}"
            f"{intro_desc_1}"
            f"{intro_desc_3}"
            f"{purple_bold(intro_desc_4)}"
        )
    )

    gen_key(f"{conf_dir}/.info.key")
    key = open_file(
        fname=f"{conf_dir}/.info.key",
        fdest="home",
        mode="rb",
    )

    email = input_single(
        in_message="\nEnter your email",
        in_type="email",
    )
    pas = input_single(
        in_message="\nEnter your password",
        in_type="password",
    )
    lo = {email: pas}
    save_yaml(
        fname=f"{conf_dir}/.cred.yaml",
        fdest="home",
        content=lo,
    )
    encrypt(
        key=key,
        fname=f"{conf_dir}/.cred.yaml",
        e_fname=f"{conf_dir}/.cred_en.yaml",
        fdest="home",
    )
    remove_file(f"{conf_dir}/.cred.yaml")
    run = input_single(
        in_message="Enter the time to run the script daily(Example: 05:00)",
        in_type="time",
    )
    numb_lines = input_single(
        in_message="Enter the number of lines from the end of log file to send",
        in_type="int",
        max_number=400,
    )
    send_list = input_list(
        subject="email address",
        description="to send to (example@gmail.com)",
        in_type="email",
    )
    log_list = input_list(
        subject="log file",
        description="to check relative to your home dir (Example: Logs/net_backup.log)",
        in_type="file",
    )
    load = {
        "runtime": run,
        "lines": int(numb_lines),
        "sendto": send_list,
        "logs": log_list,
    }
    save_yaml(
        fname=f"{conf_dir}/{conf_file_name}.yaml",
        fdest="home",
        content=load,
    )

    print(
        (
            f"{yellow_bold(statement)}"
            f"{message_1}"
            f"{cyan_bold(conf_dir_str)}"
            f"{green_bold(message_2)}"
            f"{cyan_bold(message_3)}"
        )
    )


config_setup(
    conf_dir=".config/test",
    conf_file_name="test",
)
