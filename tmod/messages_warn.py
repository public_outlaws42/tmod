#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Functions for messages warnings and errors created by me.
"""

# Standard Library Imports

# Imports from tmods
from colors import (
    cyan,
    red,
    purple,
    green,
)

__author__ = "Troy Franks"
__version__ = "2023-04-02"


def import_mod_message_from(
    imported: str,
    ifrom: str,
    methods: list[str],
) -> str:
    mod = green(ifrom)
    method = cyan(methods)
    error = red("NameError")
    im_from = red(imported)
    install = purple(f"pip install {ifrom}")
    message: str = (
        "\n"
        f"{mod} is required for {method} method(s).\n"
        f"If you are calling the method(s) you are probably\n"
        f"getting an {error} with '{im_from}' as well, if so you can install\n"
        f"{mod} with '{install}'\n"
    )
    return message


def import_mod_message(
    imported: str,
    methods: list[str],
) -> str:
    mod = green(imported)
    method = cyan(methods)
    error = red("NameError")
    install = purple(f"pip install {imported}")
    message: str = (
        "\n"
        f"{mod} is required for {method} method(s).\n"
        f"If you are calling the method(s) you are probably\n"
        f"getting an {error} with '{mod}' as well, if so you can install\n"
        f"{mod} with '{install}'\n"
    )
    return message
