#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
This is a collection of functions that converts from one to the other
"""

# Imports included with python
from datetime import datetime

# Imports installed through pip

# Imports from tmod

from file_io import open_file

__author__ = "Troy Franks"
__version__ = "2023-04-05"


def try_float(item: str):
    """
    Try's to change a string into a float.
    if it fails it returns original.
    if it converts, it returns the item as a float
    """
    try:
        flt = float(item)
    except Exception as e:
        print(e)
        flt = item
    return flt


def import_temp(fname: str = "temp.txt"):
    """
    Import temp from text file then split off each word.
    returns current temp
    """
    tempin = open_file(fname)
    templist = tempin.split()
    temp, day, hour = templist
    now_hour = datetime.now().strftime("%H.%M")
    temp_diff = round(float(now_hour) - float(hour))

    if temp_diff == 0:
        if temp > "0":
            print(f"Temp diff: {temp_diff}")
            print(f"This is the temp: {temp}")
            current_temp = temp
        else:
            current_temp = "Bad Reading"
    else:
        current_temp = "Sensor Needs Reset"
        print("Sensor needs reset")

    return current_temp
